const express = require('express');
const axios = require('axios');
const cors = require('cors');
const fs = require('fs');
const request = require('request');
const mysql = require('mysql');
const uuid = require('uuid');
const {Kafka, logLevel} = require('kafkajs');



const conn = mysql.createConnection({
    host:'localhost',
    user:'root',
    password:'',
    database:'team_squad',
    multipleStatements:true
})

conn.connect(err => {
    if(err) throw err;
    console.log("Connected to team_sqaud");
})
const kafka = new Kafka({
//     clientId:'receiver',
    brokers:['localhost:9092', 'localhost:9093', 'localhost:9094'],
    logLevel:logLevel.ERROR
})
const consumer = kafka.consumer({groupId:'upcoming', allowAutoTopicCreation:false});
consumer.connect().then(()=>console.log("Connected to TOPIC UpdatedUpcoming"));
consumer.subscribe({topic:'UpdatedUpcoming'})
const app = express();

app.use(express.json());
app.use(cors());
app.use(express.urlencoded({extended:true}));

// localhost:8003/get/squad
//used for inserting dummy mysql data from already available json file/reading , performing ops, returning dummy/previous data
app.post('/get/squad', async (req, res) =>{
    fs.readFile('./match_284.json', function(err, data){
        if(err) throw err;
        // console.log(JSON.parse(data));
        const d = JSON.parse(data).response;
        const mid = d.match_id;
        console.log(mid);

        fs.readFile(`./squad_${mid}.json`, function(err, data){
            main = JSON.parse(data).response;
            teama_id = main.teama.team_id;
            teamb_id = main.teamb.team_id;
            team_ast = main.teams;
            let a_logo = '';
            let b_logo = '';
            let p11_ids = [];
            a_name = '';
            b_name = '';
            if(team_ast[0].tid == teama_id){
                a_logo = team_ast[0].logo_url;
                a_name = team_ast[0].title;
                b_logo = team_ast[1].logo_url;
                b_name = team_ast[1].title;
            }else{
                a_logo = team_ast[1].logo_url;
                a_name = team_ast[1].title;
                b_logo = team_ast[0].logo_url;
                b_name = team_ast[0].title;
            }
            
            all_players = main.players;
            team_a_squad_prefinal = main.teama.squads;
            team_b_squad_prefinal = main.teamb.squads;
            // a_id = []
            a_final = []
            // b_id = []
            b_final = []

            // console.log("A=>",a_name,"B=>", b_name);
            team_a_squad_prefinal.forEach(item =>{
                    temp = {}
                     for(const ele of all_players){
                        //  console.log(all_players[ele].pid)
                         if(item.player_id == ele.pid){
                            temp.player_id = ele.pid;
                            temp.player_name = ele.title;
                            temp.role = ele.playing_role;
                            temp.team = a_name;
                            temp.fantasy_point = ele.fantasy_player_rating;
                            temp.nationality = ele.nationality;
                            p11_ids.push(ele.pid)
                         }
                     }
                     a_final.push(temp);
                    // a_id.push(item.player_id);
            });
            team_b_squad_prefinal.forEach(item =>{
                temp = {}
                for(const ele of all_players){
                    // console.log("B",ele.pid);
                    if(item.player_id == ele.pid){
                        temp.player_id = ele.pid;
                        temp.player_name = ele.title;
                        temp.role = ele.playing_role;
                        temp.team = b_name;
                        temp.fantasy_point = ele.fantasy_player_rating;
                        temp.nationality = ele.nationality;
                        p11_ids.push(ele.pid);
                    }
                }
                b_final.push(temp);
                    // b_id.push(item.player_id);
            });
            const final = JSON.stringify({'teama':a_final, 'teamb':b_final});
            const arr = [284, teama_id, teamb_id, a_name, b_name, a_logo, b_logo, final, 1, JSON.stringify(p11_ids)];
            console.log(arr)
            const sql = "INSERT INTO squad (match_id, teama_id, teamb_id, teama_name, teamb_name, teama_logo, teamb_logo, team, lineup_available, lineup_pids) VALUES (?)"
            conn.query(sql, [arr], function(err, results) {
                    if (err) throw err;
                    console.log("Number of records inserted: " + results.affectedRows);
                    
                });
            res.send({Status:'OK'});
        });

    })
});

app.post('/events', async (req, res) =>{
    console.log("KAFKA EVENT");
    run().then(()=> console.log('Done'), err => console.log(err));


    async function run(){
        // const kafka = new Kafka({brokers:['localhost:9092']});
    
        // const consumer = kafka.consumer({groupId:"upcoming", allowAutoTopicCreation:false});
    
        // await consumer.connect();
    
        // await consumer.subscribe({ topic:'UpdatedUpcoming', fromBeginning:true });
        await consumer.run({
            eachMessage: async({message}) => {
                console.log(JSON.parse(message.value));
            }
        });
    }
    // consumer.disconnect();
    // consumer.connect().then(console.log("Consumer Connected"));
    res.send({status:'OK'})
})

// // microservice for add_team screen with functionalities: create team, edit team, return lineup(sql view) is done. (EVENTS WITHOUT KAFKA)
// // receives event with upcoming match_ids and for each id, calls entity sport squad api to fetch and store squad info
// app.post('/events', async (req, res)=>{
//     const {type, match_ids} = req.body;
//     if(type == 'Upcoming-Updated'){
//         console.log("Event Received:",type);
//         match_ids.forEach(element => {
            
//             request.get({url:`https://rest.entitysport.com/v2/matches/${element}/squads?token=0401628019fa4df1522947816389fdf6`, json:true}, async (err, res, data)=>{
//                 if(err) throw err;
//                 // const new_data = JSON.stringify(data);
//                 // console.log(element);
//                 main = data.response;
//                 teama_id = main.teama.team_id;
//                 teamb_id = main.teamb.team_id;
//                 team_ast = main.teams;
//                 let a_logo = '';
//                 let b_logo = '';
//                 if(team_ast[0].tid == teama_id){
//                     a_logo = team_ast[0].logo_url;
//                     b_logo = team_ast[1].logo_url;
//                 }else{
//                     a_logo = team_ast[1].logo_url;
//                     b_logo = team_ast[0].logo_url;
//                 }
                
//                 all_players = main.players;
//                 team_a_squad_prefinal = main.teama.squads;
//                 team_b_squad_prefinal = main.teamb.squads;
//                 a_id = []
//                 a_final = []
//                 b_id = []
//                 b_final = []
//                 a_name = team_ast[0].title;
//                 b_name = team_ast[1].title;
//                 // console.log("A=>",a_name,"B=>", b_name);
//                 team_a_squad_prefinal.forEach(item =>{
//                         temp = {}
//                          for(const ele of all_players){
//                             //  console.log(all_players[ele].pid)
//                              if(item.player_id == ele.pid){
//                                 temp.player_id = ele.pid;
//                                 temp.player_name = ele.title;
//                                 temp.role = ele.playing_role;
//                                 temp.team = a_name;
//                                 temp.fantasy_point = ele.fantasy_player_rating;
//                                 temp.nationality = ele.nationality;
//                              }
//                          }
//                          a_final.push(temp);
//                         a_id.push(item.player_id);
//                 });
//                 team_b_squad_prefinal.forEach(item =>{
//                     temp = {}
//                     for(const ele of all_players){
//                         // console.log("B",ele.pid);
//                         if(item.player_id == ele.pid){
//                             temp.player_id = ele.pid;
//                             temp.player_name = ele.title;
//                             temp.role = ele.playing_role;
//                             temp.team = b_name;
//                             temp.fantasy_point = ele.fantasy_player_rating;
//                             temp.nationality = ele.nationality;
//                         }
//                     }
//                     b_final.push(temp);
//                         b_id.push(item.player_id);
//                 });
//                 const final = JSON.stringify({'teama':a_final, 'teamb':b_final});
//                 const arr = [element, teama_id, teamb_id, a_name, b_name, a_logo, b_logo, final]
//                 console.log(arr)
//                 const sql = "INSERT INTO squad (match_id, teama_id, teamb_id, teama_name, teamb_name, teama_logo, teamb_logo, team) VALUES (?)"
//                 conn.query(sql, [arr], function(err, results) {
//                         if (err) throw err;
//                         console.log("Number of records inserted: " + results.affectedRows);
                        
//                     });
                
//                 // console.log("A",a_final);
//                 // console.log("B",b_final);
               
//             })
            
//         });
//         res.send({status:"OK"});
//     }
// })


// localhost:8003/save/team/match/:match_id/user/:user_id/contest/:contest_id
// saves new team created by user.
app.post('/save/team/user/:user_id/match/:match_id/contest/:contest_id', async (req, res) => {
    mid = req.params.match_id;
    uid = req.params.user_id;
    cid = req.params.contest_id;
    user_team_id = `${uid}_${mid}_${cid}_${uuid.v4()}`;
    data = req.body;

    cap_id = data.captain;
    vcap_id = data.vice_captain;
    jackpot_id = data.jackpot;
    
    // console.log(cap_id, vcap_id, jackpot_id);
    const sql = "INSERT INTO user_team (user_id, match_id, contest_id, user_team_id, user_team, captain, vice_captain, jackpot) VALUES (?)"
    const val = [uid, mid, cid, user_team_id, JSON.stringify({'team':data.squad}), cap_id, vcap_id, jackpot_id]
    conn.query(sql, [val], async (error, results) =>{
        if(error) throw error;
        console.log("ROWS AFFECTED:", results.affectedRows);

        await axios.post('http://localhost:11000/events',{type:'UserCreatedTeam', data:{team_id :user_team_id, team:data.squad}});
        res.status(201).send({status:"OK", "message":`team created with id: ${user_team_id}`});
    } )

    // res.send({status:"OK"});
})

// for e.g.: localhost:8003/change/existing/team/17_284_4_1fbed454-e678-4bb3-9342-8da38d7383af
// updates the team which is already created by user
app.post('/change/existing/team/:team_id/', async (req, res) =>{
    user_team_id = req.params.team_id.toString();
    console.log(user_team_id);
    new_team = req.body;
    console.log(new_team);
    let sql = 'SELECT user_team, captain, vice_captain, jackpot FROM user_team WHERE user_team_id =' + mysql.escape(user_team_id);
    console.log(sql);
    conn.query(sql, (error, results, field) =>{
        if(error) throw error;
        // console.log(results);
        let {user_team, captain, vice_captain, jackpot} = results[0];
        new_data = JSON.parse(JSON.stringify(user_team));
        new_data = JSON.parse(new_data);
        new_data.captain = captain;
        new_data.vice_captain = vice_captain;
        new_data.jackpot = jackpot;
        if(JSON.stringify(new_data) == JSON.stringify(new_team)){
            res.send({message:"This Team already exists"});
        }else{
            console.log("In else Part");
            let sql = 'UPDATE user_team SET user_team = ' + mysql.escape(JSON.stringify(new_team)) + 'WHERE user_team_id =' + mysql.escape(user_team_id);
            conn.query(sql, (error, results, field) =>{
                if(error) throw error;
                console.log("Rows Affected:", results.affectedRows);
                res.send({message:"Team updated"})
            })
        }

        console.log(new_data);

    })
    // res.send({status:'OK'})
});

// localhost:8003/get/squad/284
// returns player data of particular match with lineup (can not retuns record for which lineup is not available <=> used mysql views)
app.post('/get/match/:id/squad', async (req, res) =>{
    const mid = req.params.id;
    const sql = `SELECT * FROM team_squad.squad_read WHERE match_id = ${mid}`;
    conn.query(sql, function(err, results, field){
        if(err) throw new err;
        final_results = results[0];
        final_response = {};
        final_response.match_id = final_results.match_id;
        final_response.teama_id = final_results.teama_id;
        final_response.teamb_id = final_results.teamb_id;
        final_response.teama_logo = final_results.teama_logo;
        final_response.teamb_logo = final_results.teamb_logo;
        let team = JSON.parse(JSON.stringify(final_results.team));
        final_response.teams = JSON.parse(team);
        final_response.lineup_pids = final_results.lineup_pids;
      
        res.status(200).send(final_response);
    });
});

// code for time based execution for getting lineups to be put here/for
app.post('/update/lineup', async (req, res)=>{
    //TODO: Select all record's id for which lineup falg is 0 and for them all, fetch lineup/squad data.
        //  or enter data sorted on timestamp of match and take first record and set timeinterval/cron to fetch lineup.
    axios.post('http://localhost:11000/events', {type:'LineupAvailable', data:{match_id:1234}});
    console.log('in lineup update url');
    res.send({status:'OK'});
})


app.listen(8003, ()=>{
    console.log("Listening on port 8003");
})