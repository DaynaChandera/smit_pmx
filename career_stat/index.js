const express = require('express');
const axios = require('axios');
const cors = require('cors');
const mysql = require('mysql');
const {Kafka, logLevel} = require('kafkajs');


const conn = mysql.createConnection({
    host:'localhost',
    user:'root',
    password:'',
    database:'career_stat',
    multipleStatements:true
});

conn.connect((err) =>{
    if(err) throw err;
    console.log('Connected To the {career_stat} database')
});

const kafka = new Kafka({
    clientId:'receiver',
    brokers:['localhost:9092', 'localhost:9093', 'localhost:9094'],
    // logLevel:logLevel.ERROR
});

// const consumer = kafka.consumer({groupId:'cg1', allowAutoTopicCreation:false});
// consumer.connect().then(console.log("Consumer Connected"));
// consumer.subscribe({topic:'CreatedUser'});
// consumer.logger()


const app = express();
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());


// localhost:8008/compare/user/2fe6fd81/current/cb020342
app.post('/compare/user/:uid/current/:cuid', async (req, res) =>{
    const uid = req.params.uid;
    const cuid = req.params.cuid;
    console.log(typeof(cuid));
    let sql = `SELECT * FROM cstat WHERE user_id = ? OR user_id = ? `; //AND user_id=${uid}
    conn.query(sql, [cuid, uid],function(err, results){
        if(err) throw err;
        res_data = JSON.parse(JSON.stringify(results));
        console.log(res_data);
        current = res_data[0].user_id == cuid ? res_data[0] : res_data[1];
        tomatch = res_data[0].user_id == uid ? res_data[0] : res_data[1];
        current_winstreak = current.total_contest / current.total_win;
        tomatch_winstreak = tomatch.total_contest / tomatch.total_win;
        current.win_streak = current_winstreak;
        tomatch.win_streak = tomatch_winstreak;

        res.send({comparision:[current,tomatch]});
    });


});

// app.post('/events', async (req, res) => {
//     console.log("Event Received on '8008'");
//     const {type, data} = req.body;
//     // consumer.subscribe('user-created')
//     if(type=='UserCreated'){
//         const {user_id, user, mobile} = data;
//         console.log('USER TO BE CREATED:', typeof(user_id), typeof(user), typeof(mobile));
//         const sql = 'INSERT INTO cstat (user_id, username, mobile) VALUES (?)'
//         conn.query(sql, [[user_id, user, mobile]], (err, results, fields) => {
//             if(err) console.log(err);
//             console.log('Records added:', results.affectedRows);
//         })
//     // possible types to be received: UserMatchParticipation, UserSportParticipation, UserContestParticipation, UserFollowing, FollowedUser, UserWin
//     }
//     if(type=='UserParticipation'){
//         const {user_id} = data;
//         const sql = 'UPDATE cstat SET total_match = total_match + 1, total_contest = total_contest + 1 WHERE user_id ='+mysql.escape(user_id);
//         conn.query(sql, (err, results)=>{
//             if(err) console.log(err);
//             console.log('RECORDS UPDATED:', results.affectedRows);
//         });
//     }
//     if(type=='UserWin'){
//         const {user_ids} = data;
//         let queries = '';
//         user_ids.forEach((element)=>{
//             queries += 'UPDATE cstat SET total_win = total_win + 1 WHERE user_id = ?;'+mysql.escape(element);
//         });
//         conn.query(queries, function(err, results){
//             if(err) throw err;
//             console.log(results.affectedRows);
//         });
//     }
//     res.send({status:"OK"});
// });

app.post('/events', async (req, res) =>{
    console.log("KAFKA EVENT");
    run().then(()=> console.log('Done'), err => console.log(err));


    async function run(){
        const kafka = new Kafka({brokers:['localhost:9092']});
    
        const consumer = kafka.consumer({groupId:"grp1"});
    
        await consumer.connect();
    
        await consumer.subscribe({ topic:'CreatedUser'});
        await consumer.subscribe({ topic:'UserParticipated'});
        await consumer.run({
            eachMessage: async({message}) => {
                console.log(JSON.parse(message.value));
            }
        });
    }
    
    // consumer.disconnect();
    // consumer.connect().then(console.log("Consumer Connected"));

    res.send({status:'OK'})
    
})


app.listen(8008, ()=>{
    console.log("Listening On Port '8008'")
});