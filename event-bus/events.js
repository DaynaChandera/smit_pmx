const {Kafka} = require('kafkajs');


const kafka = new Kafka({
    brokers:['localhost:9092', 'localhost:9093', 'localhost:9094'],
});

const producer = kafka.producer();
producer.connect();

const UpdatedUpcoming = async function(message){
        const topic = 'UpdatedUpcoming'
        const messages=[
            {key:'1',partition:0 ,value:JSON.stringify(message)},
            {key:'1',partition:1 ,value:JSON.stringify(message)},

        ]
        console.log("FROM EVENTS",{topic, messages});
        await producer.send({topic, messages, acks:1})
}

const UserCreatedTeam = async function(message){
    topic = 'UserCreatedTeam';
    const messages=[
        {value:JSON.stringify(message)}
    ]
    console.log("FROM EVENTS",{topic, messages});
    await producer.send({topic, messages});
}

const LineupAvailable = async function(message){
    const topic = 'LineupAvailable';
    const messages = [
        {partition:0, value:JSON.stringify(message)}
    ];
    console.log("LINEUP AVAILABLE", {topic, messages});
    producer.send({topic, messages});
    producer.send({topic, messages});
}

const ContestCreated = async function(message){
    const topic = 'ContestCreated';
    const messages = [
        {partition:0, value:JSON.stringify(message)}
    ];
    console.log("CONTEST CREATED", {topic, messages});
    producer.send({topic, message});
}

const CreatedUser = async function(message){
    const topic = 'CreatedUser';
    const messages = [
        {value:JSON.stringify(message)}
    ]
    console.log("CREATED USER", {topic, messages});
    producer.send({topic, messages});
}

const UserParticipated = async function(message){
    const topic = 'UserParticipated';
    const messages = [
        {partition:0, value:JSON.stringify(message)}
    ]
    console.log("USER PARTICIPATED", {topic, messages});
    producer.send({topic, messages});
    
}

module.exports = {UpdatedUpcoming, UserCreatedTeam, LineupAvailable, ContestCreated, CreatedUser, UserParticipated}