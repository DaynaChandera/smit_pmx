const mysql = require('mysql');


const config = mysql.createConnection({
    host:'localhost',
    user:'root',
    password:'',
    database:'scorecard',
    multipleStatements:true
});


config.connect((error) =>{
    if(error) throw error;
    console.log("Connected to scorecard");
})