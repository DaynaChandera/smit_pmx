const express = require('express');
const axios = require('axios');
const cors = require('cors');
const request = require('request');
const mysql = require('mysql');
const redis = require('redis');
// let config = require('./dbconfig');

const client = redis.createClient(6379);

client.on('error', (err)=>{
    console.log("error"+err);
})

const config = mysql.createConnection({
    host:'localhost',
    user:'root',
    password:'',
    database:'scorecard',
    multipleStatements:true
});



config.connect((error) =>{
    if(error) throw error;
    console.log("Connected to scorecard");
})

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

// const CACHED_SCORECARD_KEY = 'scorecard:';


// localhost:8005/get/scorecard/match/47399
app.post('/get/scorecard/match/:match_id', async (req, resp) => {
    match_id = req.params.match_id;
    console.log(typeof(match_id));
    const redis_key_scorecard = `scorecard:${match_id}`
    request.get({url:`https://rest.entitysport.com/v2/matches/${match_id}/scorecard?token=0401628019fa4df1522947816389fdf6`, json:true}, (error, res, data) =>{
        if(error) throw error;
        let innings = data.response.innings[0];
        let sts_note = data.response.status_note;
        let bats_arr = JSON.stringify(innings.batsmen);
        let bowl_arr = JSON.stringify(innings.bowlers);
        let extra = JSON.stringify(innings.extra_runs);
        let fallen_wickets = innings.fows;
        let yet_to_bet = JSON.stringify(innings.did_not_bat);
        let score = innings.scores;
        let score_full = innings.scores_full;
        fallen_wickets.forEach(element => {
            delete element['batsman_id'];  //
            delete element['runs'];  //
            delete element['balls'];  //
            delete element['how_out'];  //
            delete element['bowler_id'];  //
            delete element['dismissal'];  //
        });
        fallen_wickets = JSON.stringify(fallen_wickets);
        const arr = [match_id, sts_note, bats_arr, bowl_arr, extra, fallen_wickets, yet_to_bet, score, score_full];
        let final = {match_id:match_id, status_note: sts_note, batsman:JSON.parse(bats_arr), bowler:JSON.parse(bowl_arr), extras:JSON.parse(extras), fallen_wickets:JSON.parse(fallen_wickets), yet_to_bet:JSON.parse(yet_to_bat), scores:score, full_scores:score_full}
        client.get(redis_key_scorecard, JSON.stringify(final));
       const sql = 'INSERT INTO scorestore (match_id, status_note, batsman, bowler, extras, fallen_wickets, yet_to_bat, scores, full_scores) VALUES (?) ON DUPLICATE KEY UPDATE match_id=?, status_note=?, batsman=?, bowler=?, extras=?, fallen_wickets=?, yet_to_bat=?, scores=?, full_scores=? ';
       let qry = mysql.format(sql, [arr, match_id, sts_note, bats_arr, bowl_arr, extra, fallen_wickets, yet_to_bet, score, score_full])
       config.query(qry, (error, rows) =>{
           if(error) throw error;
           console.log("INSERT/ UPDATE Successfully");
       })
        resp.send({status:"OK"});
    })
})


//localhost:8005/read/match/:47399/scorecard
app.post('/read/match/:match_id/scorecard', async (req, resp) =>{
    const match_id = req.params.match_id;
    console.log(match_id);
    const redis_key_scorecard = `scorecard:${match_id}`
    return client.get(redis_key_scorecard, (err, result)=>{
        if(!result){
            return client.get(redis_key_scorecard, (err, result)=>{
                return resp.json({source:'cache', result})
            })
        }
        return res.json({source:'cache', result})
    })

    // const sql = `SELECT * FROM score_read WHERE match_id = ${match_id}`
    // config.query(sql, function(err, result, fields){
    //     if(err) throw err;
    //     result = result[0];
    //     // res.send(JSON.parse(JSON.stringify(result)));
    //     let final = {match_id:result.match_id, status_note: result.status_note, batsman:JSON.parse(result.batsman), bowler:JSON.parse(result.bowler), extras:JSON.parse(result.extras), fallen_wickets:JSON.parse(result.fallen_wickets), yet_to_bet:JSON.parse(result.yet_to_bat), scores:result.scores, full_scores:result.full_scores}
    //     resp.status(200).send({final});
    // })
});

app.listen(8005, ()=>{
    console.log("listening on 8005");
})

const f = function (mid, retry=3){
    const mid;
    const redis_key_scorecard=`scorecard:${mid}`;
    return client.get(redis_key_scorecard, (err, result)=>{
        if(result) return resp.json({source:'cache'});
        if(!result){ 
            if (retry>0) return f(mid, retry-1);
            return resp.json({message:"Scorecard is Currently Unavailable"});
        }
    })
}