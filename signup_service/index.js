const express = require('express');
const axios = require('axios');
const cors = require('cors');
const {randomBytes} = require('crypto');
const {Kafka} = require('kafkajs');


// const kafka = new Kafka({
//     clientId:'consumer',
//     brokers:['localhost:9092']
// });

// const consumer = kafka.consumer({groupId:'consumer-grp1'});



const app = express();
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors()); 


app.post('/create/user', async (req, res) =>{
    const data = req.body;
    user_id = randomBytes(4).toString('hex');
    data.user_id = user_id;
    event_ = {}
    event_.type = 'CreatedUser';
    event_.data = data; 
    console.log(event_);
    axios.post('http://localhost:11000/events', event_).catch((err)=>{
        if(err) throw err;
    });
    res.send("USER CREATED");
});


app.listen(8009, ()=>{
    console.log("Listening on Port '8009'");
})

